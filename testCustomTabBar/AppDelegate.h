//
//  AppDelegate.h
//  testCustomTabBar
//
//  Created by Thao on 7/28/14.
//  Copyright (c) 2014 thao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

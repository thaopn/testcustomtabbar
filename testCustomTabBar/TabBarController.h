//
//  TabBarController.h
//  testCustomTabBar
//
//  Created by Thao on 7/28/14.
//  Copyright (c) 2014 thao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarController : UITabBarController <UITabBarControllerDelegate>

@property (strong, nonatomic) UIImageView *imvItem_1;
@property (strong, nonatomic) UIImageView *imvItem_1_Selected;
@property (strong, nonatomic) UIImageView *imvItem_2;
@property (strong, nonatomic) UIImageView *imvItem_2_Selected;

@end

//
//  TabBarController.m
//  testCustomTabBar
//
//  Created by Thao on 7/28/14.
//  Copyright (c) 2014 thao. All rights reserved.
//

#import "TabBarController.h"

@interface TabBarController ()

@end

@implementation TabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.delegate = self;
    [[UITabBar appearance] setSelectionIndicatorImage:[[UIImage alloc]init]];
    
    UIImageView *imvTabBar = [[UIImageView alloc]initWithFrame:CGRectMake(0, -2, 320, 52)];
    imvTabBar.image = [UIImage imageNamed:@"botbar.png"];
    imvTabBar.userInteractionEnabled = true;
    imvTabBar.multipleTouchEnabled = true;
    [self.tabBar addSubview:imvTabBar];
    [self.tabBar insertSubview:imvTabBar atIndex:0];
    
    _imvItem_1 = [[UIImageView alloc]initWithFrame:CGRectMake(320/4-16, 49/2-20, 32, 32)];
    _imvItem_1.image = [UIImage imageNamed:@"ic_bt_back.png"];
//    _imvItem_1.userInteractionEnabled = true;
//    _imvItem_1.multipleTouchEnabled = true;
    _imvItem_1.hidden = true;
    [self.tabBar addSubview:_imvItem_1];
    
    _imvItem_2 = [[UIImageView alloc]initWithFrame:CGRectMake(3*(320/4)-16, 49/2-20, 32, 32)];
    _imvItem_2.image = [UIImage imageNamed:@"ic_bt_share.png"];
//    _imvItem_2.userInteractionEnabled = true;
//    _imvItem_2.multipleTouchEnabled = true;
    [self.tabBar addSubview:_imvItem_2];
    
    _imvItem_1_Selected = [[UIImageView alloc]initWithFrame:CGRectMake(_imvItem_1.center.x-16, _imvItem_1.center.y-16, 32, 32)];
    _imvItem_1_Selected.image = [UIImage imageNamed:@"ic_bt_back_on.png"];
    _imvItem_1_Selected.hidden = false;
    [self.tabBar addSubview:_imvItem_1_Selected];
    
    _imvItem_2_Selected = [[UIImageView alloc]initWithFrame:CGRectMake(_imvItem_2.center.x-16, _imvItem_2.center.y-16, 32, 32)];
    _imvItem_2_Selected.image = [UIImage imageNamed:@"ic_bt_share_on.png"];
    _imvItem_2_Selected.hidden = true;
    [self.tabBar addSubview:_imvItem_2_Selected];
    
//    UITabBarItem *item1 = [self.tabBarController.tabBar.items objectAtIndex:0];
//    [item1 setImage:[UIImage imageNamed:@"ic_bt_back.png"]];
    
}

-(BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    int index = [tabBarController.viewControllers indexOfObject:viewController];
    switch (index) {
        case 0:
            _imvItem_1_Selected.hidden = false;
            _imvItem_1.hidden = true;
            _imvItem_2.hidden = false;
            _imvItem_2_Selected.hidden = true;
            break;
            
        case 1:
            _imvItem_2_Selected.hidden = false;
            _imvItem_2.hidden = true;
            _imvItem_1.hidden = false;
            _imvItem_1_Selected.hidden = true;
            break;
            
        default:
            break;
    }
    
    return true;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
